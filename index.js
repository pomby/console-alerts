const STATUS = Object.freeze({
  success: 1,
  fail: 2,
  noResponse: 4,
  error: 8,
  slow: 16,
  testFail: 32,
  badConfig: 64,
});

function replaceAll(str,mapObj){
  let out = str;

  for(const rep in mapObj){
    if(mapObj.hasOwnProperty(rep)){
      out = out.replaceAll(`{${rep}}`, mapObj[rep]);
    }
  }

  return out;
}

const evalConfig = async (config, monitor, runLog, error, errorDetails) => {
  let type = 0;

  if(config.type){
    if(Array.isArray(config.type)){
      config.type.forEach(e => {
        if(STATUS[e]){
          type = type | STATUS[e];
        }
      })
    }else if(STATUS[e]){
      type = STATUS[e];
    }
  }

  if(type & runLog.status){
    // matches at least 1 status defined
    let outFunc = console.info;

    if(config.output && console[config.output]){
      outFunc = console[config.output];
    }

    if(config.template){
      outFunc(replaceAll(config.template, {
        title: monitor.title,
        runTime: runLog.runTime,
        runTimeISO: runLog.runTime ? runLog.runTime.toISOString() : '-',
        timing: runLog.timing,
        statusCode: errorDetails ? errorDetails.status : '-',
        type: monitor.type,
        category: monitor.category,
      }));
    }else{
      outFunc(`Console Alert Triggered for: ${monitor.title}`);
    }
  }
}

module.exports = {
  name: 'Console Alerts',
  onRunComplete: async (config, monitor, runLog, error, errorDetails) => {
    if(config.consoleAlerts){
      if(Array.isArray(config.consoleAlerts)){
        for(const subConf of config.consoleAlerts) {
          await evalConfig(subConf, monitor, runLog, error);
        }
      }else{
        await evalConfig(config.consoleAlerts, monitor, runLog, error);
      }
    }
  }
};

/**
 * String.prototype.replaceAll() polyfill
 * https://gomakethings.com/how-to-replace-a-section-of-a-string-with-another-one-with-vanilla-js/
 * @author Chris Ferdinandi
 * @license MIT
 */
 if (!String.prototype.replaceAll) {
	String.prototype.replaceAll = function(str, newStr){

		// If a regex pattern
		if (Object.prototype.toString.call(str).toLowerCase() === '[object regexp]') {
			return this.replace(str, newStr);
		}

		// If a string
		return this.replace(new RegExp(str, 'g'), newStr);

	};
}
