# @pomby/consolealerts

A library for use with pomby to add alerts to the console

## Sample Config

In your config you can add something like the following to your config:

```javascript
{
  "consoleAlerts": [
    {
      "type": ["fail"],
      "output": "error",
      "template": "[{runTimeISO}] There was an error: {statusCode} with monitor: {title} in category {category}"
    }
  ]
}
```

The type is the only required portion, output will default to info and template will default to `"Console Alert Triggered for: {title}"`

## Template Variables

* __title__ - Title of the monitor
* __runTime__ - Date of the run
* __runTimeISO__ - Date fo the run but formatted in ISO format
* __timing__ - Time in ms the run took
* __statusCode__ - Status code of the run (only on error)
* __type__ - Monitor Type used 
* __category__ - Monitor Category

## Types:

* success
* fail
* noResponse
* error
* slow
* testFail
* badConfig
